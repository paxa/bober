require 'minitest/autorun'
require "minitest/reporters"

Minitest::Reporters.use!(
  Minitest::Reporters::DefaultReporter.new(color: true)
)

def MiniTest.filter_backtrace(bt)
  bt
end

require_relative '../lib/bober'

Minitest.after_run do
  ExampleApp.stop!
  Bober.stop!
end

module ExampleApp
  extend self

  def start!
    return if defined?(@process_id) && @process_id
    puts "Starting example app"
    process_dir = File.expand_path('../example_app', __FILE__)
    @process_id = spawn({}, 'rackup -p 9006', chdir: process_dir)
    while (Net::HTTP.get(URI('http://localhost:4567')) rescue "") != ""
      puts "-- Waiting for altea server to start"
      sleep 0.05
    end
  end

  def stop!
    if @process_id
      puts "Stopping example server"
      Process.kill(:TERM, @process_id)
      Process.wait(@process_id)
    end
  end

end

class Minitest::Test
  include Bober::Screenshot::MinitestFailureUploader
end

