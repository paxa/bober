require_relative 'test_helper'

Bober.screenshot_method = :fog
Bober.fog_settings = {
  provider: 'backblaze',
  b2_account_id: 'xxx',
  b2_account_token: 'yyy',
  b2_bucket_name: 'test-failures',
  b2_bucket_id: 'xxxx',
}

require 'fog/backblaze'

describe "Testing wikipedia" do
  include Bober::TestDSL
  include Bober::Screenshot::MinitestFailureUploader

  it "should open wikipedia page" do
    load_page("http://wikipedia.org")

    raise "trigger screenshot"
  end
end
