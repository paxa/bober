require_relative 'test_helper'

describe "Testing double redirect" do
  include Bober::TestDSL

  before do
    ExampleApp.start!
  end

  it "should open wikipedia page" do
    load_page("http://localhost:9006/redir_page1")

    page.submit_form
    assert_equal('/redir_page4', page.path)
    assert_equal('Done !!!', page.page_text)
  end
end
