def autoinstall_gem(gem_name, version = nil)
  begin
    gem gem_name, version
  rescue Gem::LoadError
    puts "Installing #{gem_name} #{version ? version : ''}..."
    Gem.install(gem_name, version)
    gem gem_name, version
  end
end

autoinstall_gem 'sinatra', '2.0.0.rc2'
autoinstall_gem 'slim'
autoinstall_gem 'sass'

require 'sinatra/base'
require 'slim'
require 'sass'
require 'sass/plugin/rack'

Sass::Plugin.options.merge!(
  cache_location: '/tmp/sass-cache',
  full_exception: true,
  css_location: File.dirname(__FILE__) + '/public',
  template_location: File.dirname(__FILE__) + '/public/sass',
  trace_selectors: false,
  sourcemap: false,
  never_update: false
)

class BoberDemo < Sinatra::Base

  set :public_folder, settings.root + '/public'
  set :views, settings.root + '/views'
  use Sass::Plugin::Rack

  get '/' do
    slim :index
  end

  post '/show_form' do
    slim :show_form
  end

  get '/redir_page1' do
    slim :redir_page1
  end

  post '/redir_page2' do
    sleep 2
    slim :redir_page2
  end

  post '/redir_page3' do
    sleep 2
    redirect '/redir_page4'
  end

  get '/redir_page4' do
    sleep 1
    "Done !!!"
  end

end

run BoberDemo