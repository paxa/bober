require_relative 'test_helper'

describe "Testing example app" do
  include Bober::TestDSL

  before do
    ExampleApp.start!
  end

  it "should open navigate forms" do
    load_page("http://localhost:9006")

    #show_image

    assert_equal('http://localhost:9006/', page.url)
    assert_equal('Bober Demo', page.title)
    assert(page.has_content?('Testing form'))

    assert_equal('', page.field_value('#text_field'))
    assert_equal('Hello bober!', page.field_value('input[name="default_value"]'))

    page.field_set_value('#text_field', 'testing value')
    assert_equal('testing value', page.field_value('#text_field'))

    page.submit_form

    assert_equal("http://localhost:9006/show_form", page.url)
    assert(page.has_content?('testing value'))
    page.wait_for_css("a#go-back", 5, visible: false)
    page.click_link("Go Back")
    assert_equal("http://localhost:9006/", page.url)

    assert_includes(page.execute('return navigator.userAgent'), 'Mozilla/5.0')
  end

  it "should click link by title" do
    load_page("http://localhost:9006")

    page.click_link title: "Wanderland"
    assert_equal("http://localhost:9006/some_page", page.url)
  end

  it "should find element" do
    load_page("http://localhost:9006")

    elements = page.find_elements('#element_to_find')
    elements[0].click
    assert_equal(['clicked!!!'], page.alerts)
  end

  it "should find element and click on it" do
    load_page("http://localhost:9006")

    elements = page.find_elements('#element_to_find')
    elements[0].click
    assert_equal(['clicked!!!'], page.alerts)
  end
end
