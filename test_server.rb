require_relative './lib/bober'

puts "Starting server"
server = Bober::WebsocketServer.new('127.0.0.1', '9001')
while connection = server.wait_connection
  puts "Connected"
  connection.wait_handshake
  Thread.new do
    Thread.current.abort_on_exception = true
    while true
      message = connection.wait_message
      p [message.type, message.data]
      if message.type == :ping
        connection.send_message(message.data, :pong)
      elsif message.type == :close
        puts "Stoppping"
        connection.send_message(message.data, :close)
        connection.close!(false)
        break
      else
        connection.send_message(message.data, message.type)
      end
    end
  end
end