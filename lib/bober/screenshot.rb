module Bober
  module Screenshot

    def fog_upload(local_file, filename)
      if !Bober.fog_settings || Bober.fog_settings == {}
        raise "Bober.fog_settings is required for uploading screenshots"
      end

      bucket_name = Bober.fog_bucket_name || Bober.fog_settings[:bucket] || Bober.fog_settings[:b2_bucket_name]

      if !bucket_name
        raise "Bober.fog_bucket_name is required for uploading screenshots"
      end

      require 'fog/core'

      fog_connection = Fog::Storage.new(Bober.fog_settings)
      content = File.open(local_file, 'rb', &:read)

      fog_connection.put_object(
        bucket_name,
        filename,
        content
      )

      download_url = fog_connection.get_object_url(bucket_name, filename)
      Bober.logger.info "SCREENSHOT: #{download_url}"

      return download_url
    end

    def save_error_screenshot(location)
      method = Bober.screenshot_method || (RUBY_PLATFORM =~ /darwin/ ? :local : :fog)
      if method == :local
        saved_file = show_image
        @bober_saved_screenshot = saved_file if saved_file
      elsif method == :fog
        page.screenshot(file: '/tmp/bober_screenshot.png')
        location = location.sub(/\[.+?\]$/, '')
        uploaded_url = fog_upload('/tmp/bober_screenshot.png', "bober_#{location.gsub(/[^\w\d_\-\.]/, '_')}.png")
        @bober_saved_screenshot = uploaded_url if uploaded_url
      else
        Bober.logger.warn "Warning: Unknown screenshot method #{method.inspect}"
        nil
      end
    end

    def bober_saved_screenshot
      @bober_saved_screenshot
    end

    # It takes screenshot automatically when test fails
    module MinitestFailureUploader
      include Bober::Screenshot

      def teardown(*args)
        if !passed? && !skipped? && respond_to?(:show_image) && respond_to?(:page_loaded?) && page_loaded?
          begin
            #puts "Test Errors: #{failures.map(&:message)}"
            Bober.logger.info "Taking screenshot"
            save_error_screenshot(location) # if page_loaded?
            Bober.logger.info "Taking screenshot done #{location}"
          rescue => error
            Bober.logger.error "#{error.class}: #{error.message}\n#{error.backtrace.join("\n")}"
          end
        end
      end
    end
  end
end