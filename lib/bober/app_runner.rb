# TODO: error if rails app not defined

module Bober
  module AppRunner
    extend self

    attr_accessor :server_port, :server_host

    HANDLES = {
      webrick: lambda do |app, port, host|
        require 'rack/handler/webrick'
        Rack::Handler::WEBrick.run(app, Host: host, Port: port, AccessLog: [], Logger: WEBrick::Log::new(nil, 0))
      end,

      puma: lambda do |app, port, host|
        require 'rack/handler/puma'
        Rack::Handler::Puma.run(app, Host: host, Port: port, Threads: "0:4")
      end
    }

    def app
      @app || Rails.application
    end

    def app=(value)
      @app = value
    end

    def base_url
      "http://#{server_host}:#{server_port}"
    end

    def start_server
      @server_port ||= Bober::Utils.find_open_port
      @server_host ||= "127.0.0.1"

      @app_runner = Thread.new do
        Thread.current.abort_on_exception = true
        HANDLES[:puma].call(app, @server_port, @server_host)
      end
    end

    def stop_server
      @app_runner.kill
    end

    def running?
      !!@app_runner
    end
  end
end
