require 'socket'
require 'websocket/driver'
require 'json'

module Bober
  class WebsocketServer
    def initialize(host = '127.0.0.1', port = 9005)
      @server = TCPServer.new(host, port)

      @connections = Queue.new
      Bober.logger.info "Listening on #{host}:#{port}"
      start_accept_loop
    end

    def start_accept_loop
      @accept_loop = Thread.new do
        Thread.current.abort_on_exception = true
        loop do
          #looks like a client method call to open the connection
          client = @server.accept
          Bober.logger.info "Slimerjs Client Connected!"
          #if @connections.size > 0
          #  Bober.logger.info "Skip, already connected"
          #  next
          #end
          connection = Connection.new(client)
          @connections.push(connection)
        end
      end
    end

    def wait_connection
      @connections.pop
    end

    class Connection
      def initialize(client)
        @socket = client
        @socket.setsockopt(Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1)

        @driver = ::WebSocket::Driver.server(self)
        @driver.on(:connect) do |event|
          @driver.start
        end
        @driver.on(:message) do |event|
          @messages.push(event)
        end
        @driver.on(:close) do |event|
          Bober.logger.info "WebSocket connection closed #{event.reason} #{event.code}"
        end

        listen_messages

        @messages = Queue.new
        @write_lock = Mutex.new
      end

      def wait_message
        @messages.pop
      end

      def send_message(payload, type = :text)
        @write_lock.synchronize do
          if type == :text
            @driver.text(payload)
          elsif type == :ping
            @driver.ping(payload)
          elsif type == :pong
            @driver.pong(payload)
          elsif type == :close
            @driver.close(payload)
          end
        end
      end

      def write(data)
        @socket.write(data)
      end

      def listen_messages
        #puts "Listening messages"
        @listener = Thread.new do
          Thread.current.abort_on_exception = true
          begin
            while char = @socket.getc
              @driver.parse(char)
            end
          rescue => error
            Bober.logger.error error.message
            Bober.logger.error error.backtrace
          end
        end # Thread.new

        self
      end

      def close!(send_close_package = true)
        if send_close_package
          send_message("Bye bye", :close)
        end
        @listener && @listener.terminate
        @socket.close
      end
    end # class Connection
  end
end
