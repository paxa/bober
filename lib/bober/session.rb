require 'base64'
require 'json'
require 'uri'

module Bober
  class Session
    attr_reader :session_id

    def initialize(session_id, runner)
      @session_id = session_id
      @runner = runner
    end

    def url
      command(:page_url)
    end

    def path
      url = command(:page_url)
      uri = URI.parse(url)
      uri.path + (uri.query && "?#{uri.query}" || '')
    end

    def title
      command(:page_title)
    end

    def has_content?(string)
      string = Regexp.escape(string).gsub('\\ ', ' ') if string.is_a?(String)
      string = Bober::Utils.js_regexp(string) if string.is_a?(Regexp)
      command(:page_has_content, content: string)
    end

    def has_css?(selector, visible: false)
      command(:page_has_css, selector: selector, visible: visible)
    end

    def page_text
      command(:page_text)
    end

    alias_method :text, :page_text

    def page_source
      command(:page_source)
    end
    alias_method :html, :page_source

    def field_value(selector)
      command(:get_field_value, selector: selector)
    end

    def field_set_value(selector, value)
      response = command(:set_field_value, selector: selector, value: value)
      if response['status'] == 'error'
        raise DriverError, response['message']
      end
      response
    end

    def fill_in(selector, value)
      field_set_value(selector, value)
    end

    def load_url(url)
      command(:load_url, url: url)
    end

    def submit_form(selector = nil)
      command(:submit_form, selector: selector)
    end

    def click_link(title_arg = nil, selector: "a, button, input[type='submit'], input[type='button']", title: nil)
      command(:click_link, selector: selector, title: title_arg || title)
    end

    alias_method :click_button, :click_link

    def execute(code)
      command(:execute, code: code)
    end

    def execute_driver_js(code)
      command(:execute_driver_js, code: code)
    end

    def find_elements(selector, title: nil, content: nil, parent_id: nil)
      unless selector.is_a?(String)
        raise ArgumentError, "'selector' (first arg) must be a string (given #{selector.class.to_s})"
      end
      title = title || content
      response = command(:find_elements, selector: selector, title: title, parent_id: parent_id)
      if response.is_a?(Array)
        response.map do |element_data|
          Bober::Element.new(self, element_data)
        end
      else
        response
      end
    end

    # TODO: validate that only 1 element is found
    def find_element(selector, title: nil, content: nil, parent_id: nil)
      elements = find_elements(selector, title: title || content, parent_id: parent_id)

      if !elements || elements.size == 0
        #raise "Can't find element for selector: #{selector.inspect}"
        nil
      elsif elements.size > 1
        raise "More then one elements found for selector: #{selector.inspect}"
      else
        elements.first
      end
    end

    def click_element(selector_arg = nil, selector: nil, bober_id: nil)
      selector ||= selector_arg
      if selector.nil? && bober_id.nil?
        raise ArgumentError, "At least one argument is required. bober_id: or selector:"
      end
      command(:click_element, selector: selector, bober_id: bober_id)
    end

    def alerts
      command(:alerts)
    end

    def downloads
      command(:downloads)
    end

    def wait_for_url(url, timeout = nil)
      expect_url = url.dup
      url = Regexp.escape(url) if url.is_a?(String)
      url = Bober::Utils.js_regexp(url) if url.is_a?(Regexp)
      result = command(:wait_for_url, url: url, timeout: timeout)
      unless result['success']
        raise DriverError, "URL didn't appear, expected #{expect_url} actial #{result['url']}"
      end
      result
    end

    def wait_element_appear(selector, timeout_arg = nil, visible: true, timeout: nil, raise_failure: true)
      timeout ||= timeout_arg || 30
      result = command(:wait_element_appear, selector: selector, visible: visible, timeout: timeout)
      if raise_failure && !result
        raise DriverError, "Can not find element '#{selector}' (timeout: #{timeout})"
      end
      result
    end

    alias_method :wait_for_css, :wait_element_appear

    def wait_for_content(content, timeout = nil)
      content = Regexp.escape(content).gsub('\\ ', ' ') if content.is_a?(String)
      content = Bober::Utils.js_regexp(content) if content.is_a?(Regexp)
      command(:wait_for_content, content: content, timeout: timeout)
    end

    def wait_iframe_loading
      command(:wait_iframe_loading)
    end

    def wait_downloads(filename: nil, timeout: nil, raise_failure: true)
      if filename
        filename = Regexp.escape(filename).gsub('\\ ', ' ') if content.is_a?(String)
        filename = Bober::Utils.js_regexp(filename) if content.is_a?(Regexp)
      end
      result = command(:wait_downloads, filename: filename, timeout: timeout)
      if raise_failure && !result
        raise DriverError, "Browser didn't received any downloads (timeout: #{timeout})"
      end
      result
    end

    def table_data(selector, as_hashes: false)
      result = command(:table_data, selector: selector)

      data = result['table']['content']
      if as_hashes
        headers = data.shift
        data.map! do |row|
          [headers, row].transpose.to_h
        end
      end

      return data
    end

    def reset_alerts
      command(:reset_alerts)
    end

    def close
      command(:close)
    end

    def screenshot(file:)
      result = command(:screenshot)
      if result.is_a?(Hash)
        raise DriverError, result['message']
      end
      #puts response.body

      File.open(file, 'wb') do |f|
        f.write(Base64.decode64(result))
      end

      true
    end

    def command(command, args = {})
      Bober.logger.info "CMD: #{command} #{args.merge(session: @session_id)}"
      response = print_time do
        @runner.commander.send_command(command, args.merge(session: @session_id))
      end
      if response[1].is_a?(Hash) && response[1]['status'] == 'error'
        raise DriverError, response[1]['message']
      end
      response[1]
    end

    def show_image
      if RUBY_PLATFORM =~ /darwin/
        file_path = "/tmp/bober_screenshot_#{rand.to_s[-5..-1]}.png"
        screenshot(file: file_path)
        `open #{file_path}`
        return file_path
      else
        "SKIP show_image (only for Mac)"
        nil
      end
    end

    def request(method, path, options = {})
      options = Bober::Utils.merge_hash(options, query: {session: @session_id})
      result = @client.send(method, path, options)
      if result.body == 'null'
        nil
      elsif result.body == 'true'
        true
      elsif result.body == 'false'
        false
      elsif result.body == ''
        ''
      elsif result.body.start_with?('"') && result.body.end_with?('"')
        JSON.parse('[' + result.body + ']')[0]
      else
        JSON.parse(result.body)
      end
    end

    def print_time
      s_time = Time.now.to_f
      result = yield
      Bober.logger.info " -> Time #{(Time.now.to_f - s_time).round(3)} sec"
      result
    end
  end
end