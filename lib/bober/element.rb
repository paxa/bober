module Bober
  class Element < Hash

    def initialize(session, values)
      @session = session
      merge!(values)
    end

    def click
      @session.click_element(bober_id: fetch('bober_id'))
    end

    def content
      fetch('content')
    end

    def text
      fetch('content')
    end

    def find_elements(selector, title: nil)
      @session.find_elements(selector, title: title, parent_id: fetch('bober_id'))
    end

    def find_element(selector, title: nil)
      find_elements(selector, title: title, parent_id: fetch('bober_id'))[0]
    end

    def attr(attribute)
      self['attributes'][attribute.to_s]
    end

  end
end