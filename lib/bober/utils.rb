module Bober
  module Utils
    extend self

    def merge_hash(first, second)
      merger = proc do |key, v1, v2|
        Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : [:undefined, nil, :nil].include?(v2) ? v1 : v2
      end
      first.merge(second, &merger)
    end

    def js_regexp(input)
      str = input.inspect.
            sub('\\A', '^').
            sub('\\Z', '$').
            sub('\\z', '$').
            sub(/^\//, '').
            sub(/\/[a-z]*$/, '').
            gsub(/\(\?#.+\)/, '').
            gsub(/\(\?-\w+:/, '(')
      Regexp.new(str).source
    end

    def find_open_port
      require 'socket'
      socket = Socket.new(:INET, :STREAM, 0)
      socket.bind(Addrinfo.tcp("127.0.0.1", 0))
      return socket.local_address.ip_port
    ensure
      socket && socket.close
    end

    # Waiter object, blocking current thread. Better abstraction for ruby's Queue utility class
    #
    #   message_waiter = Waiter.new
    #   Thread.new { sleep 5; message_waiter.notify('Done!!!') }
    #   message_waiter.wait # => return 'Done!!!' after 5 seconds pause
    #
    class Waiter
      def initialize
        @queue = Queue.new
      end

      # Waiting for someone to call #notify
      def wait
        @queue.pop(false)
      end

      # Notify waiting side
      def notify(data)
        @queue.push(data)
      end
    end

    # It's same as waiter but support multiple subscribers
    class PubSub
      def initialize
        @waiters = []
      end

      def wait
        waiter = Waiter.new
        @waiters << waiter
        waiter.wait
      end

      def notify(value)
        while waiter = @waiters.shift
          waiter.notify(value)
        end
      end
    end
  end
end