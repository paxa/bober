module Bober
  module TestDSL

    def self.included(base)
      if defined?(Minitest::Test) && base.ancestors.include?(Minitest::Test)
        base.include(MinitestCallbacks)
        #base.instance_eval do
        #  include MinitestCallbacks
        #end
      end
    end

    def load_page(url, options = {})
      Bober.start! unless Bober.running?

      if url.start_with?('/')
        Bober.start_app_server!
        url = "#{Bober::AppRunner.base_url}#{url}"
      end

      if defined?(@page) && @page
        @page.load_url(url)
      else
        @page = Bober.create_session(url, options)
      end

      @page
    end

    def page_loaded?
      defined?(@page) && @page
    end

    def page
      unless @page
        raise "Page is not initialized, you should call load_page(url) first"
      end

      @page
    end

    def show_image
      page.show_image
    end

    module MinitestCallbacks
      def after_teardown
        if defined?(@page) && @page
          @page.close
        end
        super
      end
    end
  end
end