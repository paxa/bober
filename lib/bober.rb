require 'json'

module Bober
  autoload :Client,     File.expand_path('../bober/client', __FILE__)
  autoload :Runner,     File.expand_path('../bober/runner', __FILE__)
  autoload :Session,    File.expand_path('../bober/session', __FILE__)
  autoload :Element,    File.expand_path('../bober/element', __FILE__)
  autoload :TestDSL,    File.expand_path('../bober/test_dsl', __FILE__)
  autoload :Utils,      File.expand_path('../bober/utils', __FILE__)
  autoload :Screenshot, File.expand_path('../bober/screenshot', __FILE__)
  autoload :AppRunner,  File.expand_path('../bober/app_runner', __FILE__)
  autoload :WebsocketServer, File.expand_path('../bober/websocket_server', __FILE__)
  autoload :WebsocketCommander, File.expand_path('../bober/websocket_commander', __FILE__)

  class DriverError < StandardError; end

  extend self

  PROPS = {
    port: -1,
    fog_settings: {},
    fog_bucket_name: nil,
    screenshot_method: nil
  }

  RUN_LOCK = Mutex.new

  PROPS.each do |name, default|
    define_method(name) do
      instance_variable_defined?("@#{name}") ? instance_variable_get("@#{name}") : default
    end

    define_method("#{name}=") do |value|
      instance_variable_set("@#{name}", value)
    end
  end

  RUNNERS = {}

  def running?
    !!RUNNERS[Thread.current.object_id]
  end

  def runner
    RUNNERS[Thread.current.object_id]
  end

  def start!
    RUN_LOCK.synchronize do
      return true if running?
      RUNNERS[Thread.current.object_id] = create_runner
    end
  end

  def create_runner
    server_port = port == -1 ? Utils.find_open_port : port
    runner = Runner.new
    runner.commander = Bober::WebsocketCommander.new('127.0.0.1', server_port)
    runner.commander.start_server!
    runner.start!(port: server_port)
    runner.commander.wait_connection
    runner
  end

  def stop!
    RUNNERS.each do |thread_id, runner|
      runner.stop!
      runner.commander.close!
    end
    RUNNERS.clear
  end

  def session(start_url: nil)
    @session ||= create_session(start_url)
  end

  def create_session(start_url, options = {})
    session_runner = if options[:new_runner]
      options.delete(:new_runner)
      RUNNERS[rand] = create_runner
    else
      unless running?
        raise "Bober not running, start it with: `Bober.start!`"
      end
      runner
    end
    session_response = session_runner.commander.send_command(:create_session, load_url: start_url, options: options)
    session_id = session_response[1]['session_id']
    @session = Session.new(session_id, session_runner)
  end

  def start_app_server!
    Bober::AppRunner.start_server unless Bober::AppRunner.running?
  end

  def logger
    return @logger if @logger
    require 'logger'
    @logger = Logger.new(STDOUT)
    @logger.progname = "Bober"
    @logger.formatter = proc do |serverity, time, progname, msg|
        "#{progname}: #{msg}\n"
    end
    @logger
  end

  def logger=(new_logger)
    @logger = new_logger
  end

end