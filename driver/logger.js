var system = require('system');
var colors = require('./colors');

function log() {
  log.info.apply(log, arguments);
};

log.levels = ['error', 'warn', 'info', 'debug', 'trace', 'debug'];
log.level = system.env.BOBER_LOG || 'info';

if (!log.levels.includes(log.level)) {
  console.log(`WARN: unknown log level '${log.level}'`);
}

var canLog = function (level) {
  return log.levels.indexOf(level) <= log.levels.indexOf(log.level);
};

log.print = function (string, color) {
  system.stdout.write('BOBER ' + colors.print(string, color) + "\n");
};

log.info = function () {
  if (canLog('info')) {
    var args = Array.prototype.slice.call(arguments).map((e) => {
      return e === undefined ? '' : e.toString();
    });
    log.print(args.join(' '), 'green');
  }
};

log.warn = function () {
  if (canLog('warn')) {
    var args = Array.prototype.slice.call(arguments).map((e) => {
      return e === undefined ? '' : e.toString();
    });
    log.print(args.join(' '), 'yellow');
  }
};

log.error = function () {
  if (canLog('error')) {
    var args = Array.prototype.slice.call(arguments).map((e) => {
      return e === undefined ? '' : e.toString();
    });
    log.print(args.join(' '), 'red');
  }
};

log.debug = function () {
  if (canLog('debug')) {
    var args = Array.prototype.slice.call(arguments).map((e) => {
      return e === undefined ? '' : e.toString();
    });
    log.print(args.join(' '), 'blue');
  }
};

log.trace = function () {
  if (canLog('trace')) {
    var args = Array.prototype.slice.call(arguments).map((e) => {
      return e === undefined ? '' : e.toString();
    });
    log.print(args.join(' '), 'gray');
  }
};

module.exports = log;