var system = require('system');
var WebPageServer = require('./server');
var log = require('./logger');

var pageServer = new WebPageServer();

var router = {
  run(socket, message) {
    var rid = message[0];
    var command = message[1];
    var args = message[2];

    log.debug(`REQ (rid: ${rid}) ${command}`);

    try {
      if (command == 'create_session') {
        pageServer.createSession(args.load_url, args.options || {}, (session) => {
          this.respond(socket, rid, { status: 'success', session_id: session.sessionId });
        });
      } else if (command == 'ping') {
        this.respond(socket, rid, 'pong');
      } else if (command == 'quit') {
        this.respond(socket, rid, 'ok');
        socket.close();
        slimer.exit();
      } else {
        var result = WebPageServer.processCommand(command, args.session, args);
        if (result == WebPageServer.NOT_FOUND) {
          this.respond(socket, rid, { status: 'error', message: "Command not Found" });
        } else {
          Promise.resolve(result).then((value, error) => {
            try {
              this.respond(socket, rid, value);
            } catch (error) {
              log.error(error);
              log.error(error.message);
              log.error(error.stack);
              this.respond(socket, rid, error.message);
            }
          });
        }
      }
    } catch (error) {
      log.error(error);
      log.error(error.message);
      log.error(error.stack);
      this.respond(socket, rid, error.message);
    }
  },

  respond(socket, rid, data) {
    log.debug(`RES (rid: ${rid})`);
    socket.send(JSON.stringify([rid, data]));
  }
};

var port = system.env.PORT ? parseInt(system.env.PORT) : 9005;

//log.error("starting server on port", port);
//log.warn("starting server on port", port);
log.info("starting server on port", port);
//log.debug("starting server on port", port);
//log.trace("starting server on port", port);

var socket = new WebSocket(`ws://127.0.0.1:${port}/`);

socket.onopen = (e) => {
  //console.log('websocket ready', e);
};

socket.onmessage = (event) => {
  //console.log('websocket message', event.data);
  var message = JSON.parse(event.data);
  router.run(socket, message);
};


socket.onerror = (event, error) => {
  console.log('socket.onerror');
  console.log('websocket error', event, error, wsCodeTitle(event.code));
};

socket.onclose = function (event) {
  console.log('socket.onclose');
  console.log('websocket close', event.code, wsCodeTitle(event.code));
};

var wsCodeTitle = function(code) {
  if (code == 1000) {
    return "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
  } else if(event.code == 1001) {
    return "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
  } else if(event.code == 1002) {
    return "An endpoint is terminating the connection due to a protocol error";
  } else if(event.code == 1003) {
    return "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
  } else if(event.code == 1004) {
    return "Reserved. The specific meaning might be defined in the future.";
  } else if(event.code == 1005) {
    return "No status code was actually present.";
  } else if(event.code == 1006) {
    return "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
  } else if(event.code == 1007) {
    return "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
  } else if(event.code == 1008) {
    return "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
  } else if(event.code == 1009) {
    return "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
  } else if(event.code == 1010) {
    return "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
  } else if(event.code == 1011) {
    return "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
  } else if(event.code == 1015) {
    return "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
  } else {
    return "Unknown reason";
  }
  
}