var system = require('system');
var qs = require('./qs');
var WebPageServer = require('./server');
var webserver = require("webserver").create();
var log = require('./logger');

function renderJson(response, value, statusCode = 200) {
  response.statusCode = statusCode;
  response.setHeader('Content-Type', 'application/json');
  response.write(JSON.stringify(value, null, 2));
  response.close();
}


var pageServer = new WebPageServer();

var port = system.env.PORT ? parseInt(system.env.PORT) : 9005;

//log.error("starting server on port", port);
//log.warn("starting server on port", port);
log.info("starting server on port", port);
//log.debug("starting server on port", port);
//log.trace("starting server on port", port);

webserver.listen(`127.0.0.1:${port}`, (request, response) => {

  try {
    var startTime = Date.now();
    var method = request.method, path = request.path;
    var query = qs.parse(request.queryString);

    log.debug(" -> ", method, path, JSON.stringify(query));

    if (method == 'POST' && path == '/session') {
      pageServer.createSession(query.load_url, query.options || {}, (session) => {
        renderJson(response, {
          status: 'success',
          session_id: session.sessionId
        });
      });
    } else if (method == 'GET' && path == '/ping') {
      response.write('pong');
      response.close();
    } else if (method == 'GET' && path == '/quit') {
      response.write('pong');
      response.close();

      log.info("Stopping slimer server");
      pageServer.shutDown();
      webserver.close();
      slimer.exit();
    } else {
      var sessionId = request.headers['X-Bober-Session'] || query.session;
      var result = WebPageServer.processCommand(request.path.replace(/^\//, ''), sessionId, query);
      if (result == WebPageServer.NOT_FOUND) {
        response.statusCode = 404;
        response.setHeader('Content-Type', 'application/json');
        response.write(JSON.stringify({success: false, message: "Page not Found", request: request}, null, 2));
        response.close();
        //console.log("--> req time:", Date.now() - startTime, 'ms');
      } else {
        Promise.resolve(result).then(function(value) {
          renderJson(response, value);
          //console.log("--> req time:", Date.now() - startTime, 'ms');
        });
      }
    }
  } catch (error) {
    log.error(error);
    log.error(error.message);
    log.error(error.stack);
    renderJson(response, {
      status: 'error',
      message: error.message,
      stack: error.stack
    }, 400);
  }
});
