var fs = require('fs');
var WebSession = require('./web_session');
var log = require('./logger');

class WebPageServer {
  constructor() {
  }

  createSession(loadUrl, options, callback) {
    var sessionId = (Date.now() + parseInt((Math.random() * 10000).toFixed(0))).toString(36);
    var session = WebSession.Sessions[sessionId] = new WebSession(sessionId);

    if (loadUrl) {
      session.loadPage(loadUrl, options, callback);
    } else {
      callback(session);
    }

    return session;
  }

  static findSession(sessionId) {
    if (sessionId) {
      if (WebSession.Sessions[sessionId]) {
        return WebSession.Sessions[sessionId];
      } else {
        throw new Error(`unknown session '${sessionId}', registered: ${Object.keys(WebSession.Sessions).join(', ')}`);
      }
    } else {
      throw new Error("session is required");
    }
  }

  static processCommand(command, sessionId, args = {}) {
    var session = this.findSession(sessionId);

    switch (command) {
      case 'page_url': return session.pageUrl();
      case 'page_title': return session.pageTitle();
      case 'page_source': return session.pageSource();
      case 'page_text': return session.pageText();
      case 'page_has_content': return session.pageHasContent(args.content);
      case 'page_has_css': return session.pageHasCss(args.selector, args.visible);
      case 'screenshot': return session.getScreenshot();
      case 'get_field_value': return session.getFieldValue(args.selector);
      case 'set_field_value': return session.setFieldValue(args.selector, args.value);
      case 'submit_form': return session.submitForm(args.selector);
      case 'click_link': return session.clickLink(args.selector, args.title);
      case 'find_elements': return session.findElements(args.selector, args.title, args.parent_id);
      case 'click_element': return session.clickElement(args.selector, args.bober_id);
      case 'alerts': return session.alerts;
      case 'execute': return session.executeJs(args.code, args.args);
      case 'execute_driver_js': return session.executeDriverJs(args.code);
      case 'downloads': return session.downloads;
      case 'wait_for_url': return session.waitForUrl(args.url, args.timeout);
      case 'wait_element_appear': return session.waitElementAppear(args.selector, args.visible, args.timeout);
      case 'wait_for_content': return session.waitForContent(args.content, args.timeout);
      case 'wait_iframe_loading': return session.waitIframeLoading();
      case 'wait_downloads': return session.waitDownloads(args.filename, args.timeout);
      case 'table_data': return session.getTableData(args.selector);
      case 'load_url': return session.loadUrl(args.url);
      case 'reset_alerts': return session.resetAlerts();
      case 'close': return session.close(true);
    }
    return WebPageServer.NOT_FOUND;
  }

  shutDown() {
    Object.keys(WebSession.Sessions).forEach((key) => {
      WebSession.Sessions[key].close();
    });
    WebPageServer.clearDownloads();
  }

  static downloadsFolder() {
    if (!fs.exists(WebPageServer.tmpFolder)) {
      fs.makeDirectory(WebPageServer.tmpFolder);
    }
    return WebPageServer.tmpFolder;
  }

  static clearDownloads() {
    if (fs.exists(WebPageServer.tmpFolder)) {
      fs.removeTree(WebPageServer.tmpFolder);
    }
  }
}

WebPageServer.tmpFolder = `/tmp/slimerjs-${Date.now()}`;
WebPageServer.NOT_FOUND = '~~~~~~!!!!^^^';

module.exports = WebPageServer;
