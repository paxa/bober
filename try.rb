require_relative './lib/bober'

Bober.start!

at_exit {
  puts "Stopping Bober!"
  Bober.stop!
}

while true
  begin
    p Bober::WebsocketCommander.send_command(:ping)
  rescue => error
    puts error.message
    puts error.backtrace
    raise error
  end
end