require_relative '../lib/bober'

Bober.start!
at_exit { Bober.stop! }

session1 = Bober.create_session("https://www.google.com")

puts session1.execute("return document.cookie")
puts session1.execute("document.cookie = 'key111=value'")
puts session1.execute_driver_js("return JSON.stringify(this.webpage.cookies, null, 4)")
#session1.show_image

session2 = Bober.create_session("https://www.google.com", new_runner: true)
puts session2.execute_driver_js("return JSON.stringify(this.webpage.cookies, null, 4)")